package com.skobbler.debugkit.debugsettings;

import android.content.Context;

import com.skobbler.debugkit.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandra on 8/29/16.
 */
public class WorldLayerTileServer extends SingleChoiceListDebugSettings {


    @Override
    List<String> defineChoices() {
        List<String> choices = new ArrayList<String>();
        Context context=specificLayout.getContext();
        choices.add(context.getResources().getString(R.string.world_layer_OSM));
        choices.add(context.getResources().getString(R.string.world_layer_staeman));
        return choices;
    }

    @Override
    int defineInitialSelectionIndex() {
        return 0;
    }


    @Override
    int defineSpecificLayout() {
        return R.layout.layout_world_layer_tile;
    }
}
