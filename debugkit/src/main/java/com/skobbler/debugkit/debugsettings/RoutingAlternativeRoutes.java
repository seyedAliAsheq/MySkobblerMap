package com.skobbler.debugkit.debugsettings;

import android.content.Context;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.skobbler.debugkit.R;
import com.skobbler.ngx.routing.SKRouteSettings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cristianp on 10/12/16.
 */

public class RoutingAlternativeRoutes extends DebugSettings {

    /**
     * Route mode 1
     */
    public static SKRouteSettings.SKRouteMode routeMode1 = SKRouteSettings.SKRouteMode.CAR_FASTEST;
    /**
     * Route mode 2
     */
    public static SKRouteSettings.SKRouteMode routeMode2 = SKRouteSettings.SKRouteMode.CAR_FASTEST;
    /**
     * Number of the of whom the mode is modified
     */
    public static int alternative_route_selected = 0;

    @Override
    List<Pair<String, Object>> defineKeyValuePairs() {
        Context context = specificLayout.getContext();
        List<Pair<String, Object>> keyValuePairs = new ArrayList<Pair<String, Object>>();
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.routing_alternative_route_mode1), routeMode1));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.routing_alternative_route_mode2), routeMode2));
        return keyValuePairs;
    }

    @Override
    int defineSpecificLayout() {
        return R.layout.routing_alternative_routes;
    }

    @Override
    void defineSpecificListeners() {

        specificLayout.findViewById(R.id.routing_alternative_route_mode1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alternative_route_selected = 1;
                DebugSettings.getInstanceForType(RoutingRouteMode.class).open(debugBaseLayout, RoutingAlternativeRoutes.this);
            }
        });

        specificLayout.findViewById(R.id.routing_alternative_route_mode2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alternative_route_selected = 2;
                DebugSettings.getInstanceForType(RoutingRouteMode.class).open(debugBaseLayout, RoutingAlternativeRoutes.this);
            }
        });
    }

    @Override
    void onChildChanged(DebugSettings changedChild) {
        super.onChildChanged(changedChild);
        TextView routeMode1TF = (TextView) specificLayout.findViewById(R.id.routing_alternative_route_mode1).findViewById(R.id.property_value);
        TextView routeMode2TF = (TextView) specificLayout.findViewById(R.id.routing_alternative_route_mode2).findViewById(R.id.property_value);

        if (alternative_route_selected == 1) {
            switch (((RoutingRouteMode) changedChild).getCurrentSelectedIndex()) {
                case 0:
                    routeMode1 = SKRouteSettings.SKRouteMode.CAR_SHORTEST;
                    break;
                case 1:
                    routeMode1 = SKRouteSettings.SKRouteMode.CAR_FASTEST;
                    break;
                case 2:
                    routeMode1 = SKRouteSettings.SKRouteMode.EFFICIENT;
                    break;
                case 3:
                    routeMode1 = SKRouteSettings.SKRouteMode.PEDESTRIAN;
                    break;
                case 4:
                    routeMode1 = SKRouteSettings.SKRouteMode.BICYCLE_FASTEST;
                    break;
                case 5:
                    routeMode1 = SKRouteSettings.SKRouteMode.BICYCLE_SHORTEST;
                    break;
                case 6:
                    routeMode1 = SKRouteSettings.SKRouteMode.BICYCLE_QUIETEST;
                    break;
            }
            routeMode1TF.setText(routeMode1.toString());
        } else if (alternative_route_selected == 2) {
            switch (((RoutingRouteMode) changedChild).getCurrentSelectedIndex()) {
                case 0:
                    routeMode2 = SKRouteSettings.SKRouteMode.CAR_SHORTEST;
                    break;
                case 1:
                    routeMode2 = SKRouteSettings.SKRouteMode.CAR_FASTEST;
                    break;
                case 2:
                    routeMode2 = SKRouteSettings.SKRouteMode.EFFICIENT;
                    break;
                case 3:
                    routeMode2 = SKRouteSettings.SKRouteMode.PEDESTRIAN;
                    break;
                case 4:
                    routeMode2 = SKRouteSettings.SKRouteMode.BICYCLE_FASTEST;
                    break;
                case 5:
                    routeMode2 = SKRouteSettings.SKRouteMode.BICYCLE_SHORTEST;
                    break;
                case 6:
                    routeMode2 = SKRouteSettings.SKRouteMode.BICYCLE_QUIETEST;
                    break;
            }
            routeMode2TF.setText(routeMode2.toString());
        }
    }

}
