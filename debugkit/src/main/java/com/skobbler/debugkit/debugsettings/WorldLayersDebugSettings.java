package com.skobbler.debugkit.debugsettings;

import android.content.Context;
import android.nfc.Tag;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.skobbler.debugkit.R;
import com.skobbler.ngx.SKMaps;
import com.skobbler.ngx.map.SKBoundingBox;
import com.skobbler.ngx.map.worldlayer.SKWorldLayerDownloadListener;
import com.skobbler.ngx.map.worldlayer.SKWorldLayerManager;
import com.skobbler.ngx.map.worldlayer.SKWorldLayerSettings;
import com.skobbler.ngx.navigation.SKNavigationListener;
import com.skobbler.ngx.navigation.SKNavigationManager;
import com.skobbler.ngx.navigation.SKNavigationState;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexandra on 8/26/16.
 */
public class WorldLayersDebugSettings extends DebugSettings {

    /**
     * Layer unique name, must be less than 200 chars and must not contain '...'
     */
    private String uniqueName = "staeman";

    /**
     * URL of the tile server( image url= tileServerUrl/zoom/x/y.png)
     */
    private String tileServerURL = "http://b.b.tile.stamen.com/terrain-background";

    /**
     * The non zero rendering order (zero is the street level, above zero will
     * be rendered on top of the streets), must be different per layer
     */
    private int orderIDx = -2;

    /**
     * Cache size for the layer, in bytes
     */
    private int cacheSize = 50 * 1024 * 1024;

    /**
     * Transparency of the layer (1 - opaque, 0 - fully transparent)
     */
    private float trasnparency = 0.5f;

    /**
     * The images from layer X will be shown at zoom (X + zoomAdjust)
     */
    private int zoomAdjust = 0;

    /**
     * min (engine) zoom where the layer will appear
     */
    private int minZoomLevel = 1;

    /**
     * max (engine) zoom where the layer will appear
     */
    private int maxZoomLevel = 17;

    @Override
    List<Pair<String, Object>> defineKeyValuePairs() {
        Context context = specificLayout.getContext();
        List<Pair<String, Object>> keyValuePairs = new ArrayList<Pair<String, Object>>();
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_settings), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_unique_name), uniqueName));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_tile_server), tileServerURL));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_order_idX), orderIDx));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_cache_size), cacheSize));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_transparency), trasnparency));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_zoom_adjust), zoomAdjust));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_min_zoom_level), minZoomLevel));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_max_zoom_level), maxZoomLevel));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_actions), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_enable), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_disable), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_add), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_remove), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_remove_data), null));
        keyValuePairs.add(new Pair<String, Object>(context.getResources().getString(R.string.world_layer_download_data), null));



        return keyValuePairs;
    }

    @Override
    int defineSpecificLayout() {
        return R.layout.layout_world_layers;
    }

    @Override
    void defineSpecificListeners() {

        final EditText uniqueName = (EditText) specificLayout.findViewById(R.id.unique_name_layer).findViewById(R.id.property_value);
        final EditText orderIDx = (EditText) specificLayout.findViewById(R.id.order_idx_layer).findViewById(R.id.property_value);
        final EditText cacheSize = (EditText) specificLayout.findViewById(R.id.cache_size_layer).findViewById(R.id.property_value);
        final EditText trasnparency = (EditText) specificLayout.findViewById(R.id.transparency_layer).findViewById(R.id.property_value);
        final EditText zoomAdjust = (EditText) specificLayout.findViewById(R.id.zoom_adjust_layer).findViewById(R.id.property_value);
        final EditText minZoomLevel = (EditText) specificLayout.findViewById(R.id.min_zoom_layer).findViewById(R.id.property_value);
        final EditText maxZoomLevel = (EditText) specificLayout.findViewById(R.id.max_zoom_layer).findViewById(R.id.property_value);

        specificLayout.findViewById(R.id.tile_server_layer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DebugSettings.getInstanceForType(WorldLayerTileServer.class).open(debugBaseLayout, WorldLayersDebugSettings.this);
            }
        });
        specificLayout.findViewById(R.id.enable_world_layer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SKWorldLayerManager.getInstance().enableWorldLayer();
            }
        });
        specificLayout.findViewById(R.id.disable_world_layer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SKWorldLayerManager.getInstance().disableWorldLayer();
            }
        });
        specificLayout.findViewById(R.id.add_world_layer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SKWorldLayerSettings worldLayerSettings = new SKWorldLayerSettings(uniqueName.getText().toString(), tileServerURL);
                worldLayerSettings.setOrderIDx(Integer.parseInt(orderIDx.getText().toString()));
                worldLayerSettings.setCacheSize(Integer.parseInt(cacheSize.getText().toString()));
                worldLayerSettings.setZoomAadjust(Integer.parseInt(zoomAdjust.getText().toString()));
                worldLayerSettings.setTrasnparency(Float.parseFloat(trasnparency.getText().toString()));
                worldLayerSettings.setMinZoomLevel(Integer.parseInt(minZoomLevel.getText().toString()));
                worldLayerSettings.setMaxZoomLevel(Integer.parseInt(maxZoomLevel.getText().toString()));
                SKWorldLayerManager.getInstance().addWorldLayer(worldLayerSettings);

            }
        });
        specificLayout.findViewById(R.id.remove_world_layer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SKWorldLayerManager.getInstance().removeWorldLayer(uniqueName.getText().toString(), false);
            }
        });
        specificLayout.findViewById(R.id.remove_data_world_layer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int[] zoomLevels = {Integer.parseInt(minZoomLevel.getText().toString()), Integer.parseInt(maxZoomLevel.getText().toString())};
                SKWorldLayerManager.getInstance().removeWorldLayerData(uniqueName.getText().toString(), zoomLevels, false);
            }
        });

        specificLayout.findViewById(R.id.download_data_world_layer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SKWorldLayerManager.getInstance().setWorldLayerListener(new SKWorldLayerDownloadListener() {
                    @Override
                    public void onWorldLayerDownloaded(String s, SKBoundingBox skBoundingBox, int i, int i1) {
                        Log.d("WorldLayer download ", s + "  " + skBoundingBox.toString() + " min zoom = " + i + "max zoom = " + i1);

                    }
                });
                SKWorldLayerManager.getInstance().setMapView(activity.getMapView());
                SKWorldLayerManager.getInstance().downloadWorldLayer(uniqueName.getText().toString(), activity.getMapView().getBoundingBoxForRegion(activity.getMapView().getCurrentMapRegion()), 15, 17);

            }
        });

    }

    @Override
    void onChildChanged(DebugSettings changedChild) {
        super.onChildChanged(changedChild);
        if (changedChild instanceof WorldLayerTileServer) {
            switch (((WorldLayerTileServer) changedChild).getCurrentSelectedIndex()) {
                case 0:
                    ((TextView) specificLayout.findViewById(R.id.tile_server_layer).findViewById(R.id.property_value)).setText("OSM");
                    tileServerURL = specificLayout.getContext().getString(R.string.world_layer_OSM);
                    break;
                case 1:
                    ((TextView) specificLayout.findViewById(R.id.tile_server_layer).findViewById(R.id.property_value)).setText("Staeman");
                    tileServerURL = specificLayout.getContext().getString(R.string.world_layer_staeman);
                    break;

            }
        }
    }

}
